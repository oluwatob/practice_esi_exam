package com.rentit.sales.application.dto;

import com.rentit.common.rest.ResourceSupport;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by omitobisam on 16.06.16.
 */
@Data
@NoArgsConstructor(force = true)
public class CustomerDTO  extends ResourceSupport {
    Long _id;
    String name;
    String location;
    String email;
}
