package com.rentit.sales.application.service;

import com.rentit.common.application.dto.BusinessPeriodDTO;
import com.rentit.common.application.exceptions.PlantNotFoundException;
import com.rentit.common.rest.ExtendedLink;
import com.rentit.inventory.application.service.InventoryService;
import com.rentit.sales.application.dto.POLineItemDTO;
import com.rentit.sales.application.dto.PurchaseOrderDTO;
import com.rentit.sales.domain.model.PurchaseOrder;
import com.rentit.sales.domain.model.PurchaseOrderLineItem;
import com.rentit.sales.rest.PurchaseOrderRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.PATCH;

@Service
public class POLIneItemAssembler extends ResourceAssemblerSupport<PurchaseOrderLineItem, POLineItemDTO> {
    @Autowired
    InventoryService inventoryService;

    public POLIneItemAssembler() {
        super(PurchaseOrderRestController.class, POLineItemDTO.class);
    }

    @Override
    public POLineItemDTO toResource(PurchaseOrderLineItem poLineItem) {
        POLineItemDTO dto = createResourceWithId(poLineItem.getId().getId(), poLineItem);
        try {
            dto.setPlant(inventoryService.findPlant(poLineItem.getPlant()));
        } catch (PlantNotFoundException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        dto.set_id(poLineItem.getId().getId());
        dto.setRentalPeriod(BusinessPeriodDTO.of(poLineItem.getRentalPeriod().getStartDate(), poLineItem.getRentalPeriod().getEndDate()));
//        dto.setTotal(purchaseOrder.getTotal());
        dto.setStatus(poLineItem.getStatus());

        try {
            switch (poLineItem.getStatus()) {
                case PENDING:
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(PurchaseOrderRestController.class).acceptPurchaseOrder(dto.get_id())).toString(),
                            "accept", PATCH));
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(PurchaseOrderRestController.class).rejectPurchaseOrder(dto.get_id())).toString(),
                            "reject", DELETE));
                    break;
                case OPEN:
                    dto.add(new ExtendedLink(
                            linkTo(methodOn(PurchaseOrderRestController.class).closePurchaseOrder(dto.get_id())).toString(),
                            "close", DELETE));
                    break;
            }
        } catch (Exception e) {}
        return dto;
    }
}
