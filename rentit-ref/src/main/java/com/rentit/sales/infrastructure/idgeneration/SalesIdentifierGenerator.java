package com.rentit.sales.infrastructure.idgeneration;

import com.rentit.common.infrastructure.HibernateBasedIdentifierGenerator;
import com.rentit.sales.domain.model.CustomerID;
import com.rentit.sales.domain.model.PurchaseOrderID;
import com.rentit.sales.domain.model.PurchaseOrderLineItemID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SalesIdentifierGenerator {
    @Autowired
    HibernateBasedIdentifierGenerator hibernateGenerator;

    public PurchaseOrderID nextPurchaseOrderID() {
        return PurchaseOrderID.of(hibernateGenerator.getID("PurchaseOrderIDSequence"));
    }
    public PurchaseOrderLineItemID nextPurchaseOrderLineItemID() {
        return PurchaseOrderLineItemID.of(hibernateGenerator.getID("PurchaseOrderLineItemIDSequence"));
    }
    public CustomerID nextCustomerID() {
        return CustomerID.of(hibernateGenerator.getID("CustomerIDSequence"));
    }
}
