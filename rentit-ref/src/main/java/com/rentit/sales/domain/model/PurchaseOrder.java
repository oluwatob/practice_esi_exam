package com.rentit.sales.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.PlantInventoryEntry;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.inventory.domain.model.PlantReservation;
import com.rentit.inventory.domain.model.PlantReservationID;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class PurchaseOrder {
    @EmbeddedId
    PurchaseOrderID id;

    CustomerID customerID;

    ContactPerson contactPerson;

    @OneToMany(cascade = CascadeType.ALL)
    List<PurchaseOrderLineItem> poLineItem;

    @Column(precision = 8, scale = 2)
    BigDecimal total;

    public static PurchaseOrder of(PurchaseOrderID id, PlantInventoryEntryID plant, BusinessPeriod period) {
        PurchaseOrder po = new PurchaseOrder();
        po.id = id;
        return po;
    }

    public void confirmReservation(PlantReservationID reservation, BigDecimal plantPrice) {
      poLineItem.get(1).reservations.add(reservation);
        total = plantPrice.multiply(BigDecimal.valueOf(poLineItem.get(1).rentalPeriod.numberOfWorkingDays()));
        poLineItem.get(1).status = POStatus.OPEN;
    }

}
