package com.rentit.sales.domain.model;

import com.rentit.common.domain.model.BusinessPeriod;
import com.rentit.inventory.domain.model.PlantInventoryEntryID;
import com.rentit.inventory.domain.model.PlantReservationID;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by omitobisam on 16.06.16.
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class PurchaseOrderLineItem {

    @EmbeddedId
    PurchaseOrderLineItemID id;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="plant_id"))})
    PlantInventoryEntryID plant;
    @Embedded
    BusinessPeriod rentalPeriod;

    @ElementCollection
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="reservation_id"))})
    List<PlantReservationID> reservations = new ArrayList<>();

    LocalDate issueDate;
    LocalDate paymentSchedule;

    @Enumerated(EnumType.STRING)
    POStatus status;


    public void accept() {
        if (status.equals(POStatus.PENDING))
            status = POStatus.OPEN;
    }

    public void reject() {
        if (status.equals(POStatus.PENDING))
            status = POStatus.REJECTED;
    }

    public void close() {
        if (status.equals(POStatus.OPEN))
            status = POStatus.CLOSED;
    }


}
