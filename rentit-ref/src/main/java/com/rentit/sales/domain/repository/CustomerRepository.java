package com.rentit.sales.domain.repository;

import com.rentit.sales.domain.model.Customer;
import com.rentit.sales.domain.model.CustomerID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by omitobisam on 16.06.16.
 */
@Repository
public interface CustomerRepository extends JpaRepository<Customer, CustomerID>{
}
