package com.rentit.sales.domain.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

/**
 * Created by omitobisam on 16.06.16.
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED, force = true)
public class Customer {
    @EmbeddedId
    CustomerID id;

    String name;
    String location;
    String email;
}
